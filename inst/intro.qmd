# Introduction to R

This is a very short introduction to R to help the reader follow the content of the notes and tutorials.
We recommend @torfs_very_nodate and @zimmerman2019 for a longer introduction covering different parts of R.

## Installation

Download and install R following the information at <https://cran.rstudio.com/>.
Also, download and install RStudio Desktop following the information at <https://www.rstudio.com/products/rstudio/download/#download>.

## RStudio Layout

RStudio is organise in a $2 \times 2$ grid layout.
By default, you have

-   source pane (top left)
-   console pane (bottom left)
-   R environment (top right)
-   file navigation (bottom right)

The console pane is where you can type and run R code.
`>` at the console pane means that it is waiting for new instructions.
`+` at the console pane means that the previous instruction is missing something.
You can return to `>` from `+` by pressing the `Esc` key in your keyboard.

More about RStudio in the [RStudio cheatsheet](https://raw.githubusercontent.com/rstudio/cheatsheets/main/rstudio-ide.pdf).

## Libraries

R has a large collection of additional functionalities created by users.
In the notes and tutorials, we will use the [`tidyverse`](https://www.tidyverse.org/) library.

To install a library, run

```{r}
#| eval: false

install.packages('tidyverse')
```

in the console pane.

To load a library to use, run

```{r}
#| eval: false

library('tidyverse')
```

in the console pane.

## Variables

You can store data into variables.
For example,

```{r}
x <- 0
```

defines the variable `x` that store the data `0`.

## Functions

Function is a set of pre-define commands to process a input data and output new data.
For example,

```{r}
sum(1, 1)
```

executes the function `sum()` that process the input `1` and `1` and output `2`.

Functions can be connected using the forward pipe (`|>`).
For example,

```{r}
sum(1, 1) |>
  sqrt()
```

executes the function `sum()` that process the input `1` and `1` and output `2` that is passed to the function `sqrt()` and output `r sqrt(2)`.

## Read CSV files

The [`tidyverse`](https://www.tidyverse.org/) library provides the function `read_csv()`.
For example,

```{r}
#| eval: false

df <- read_csv('awesome-data.csv')
```

will read the file `awesome-data.csv` and store it in the variable `df`.

::: callout-important
## File does not exist in current working directory

`read_csv()` will try to read the file relative to the current working directory.
Use `getwd()` to find out your current working directory.
If the file isn't in the current working directory, you can include the relative path from the current working directory to the file (for example, `data/folder/awesome-data.csv`) or change the current working directory using `setwd()`.
:::
