# CityU 2022-09 PH5102: Introduction to Biostatistics in One Health

Course Information page: https://www.cityu.edu.hk/catalogue/pg/202223/course/PH5102.htm

Practical Exercises as

- web pages at https://raniere-phd.gitlab.io/cityu-2022-09-ph5102
- Microsoft Word Document (`.docx`) at https://raniere-phd.gitlab.io/cityu-2022-09-ph5102/Introduction-to-Biostatistics-in-One-Health--Practical-Execises.docx
