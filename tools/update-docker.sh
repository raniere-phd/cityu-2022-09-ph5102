#!/bin/bash
DOCKER_IMAGE="registry.gitlab.com/raniere-phd/cityu-2022-09-ph5102"
SERVICE_NAME="ci"

docker build \
       --no-cache \
       --pull \
       --quiet \
       --target $SERVICE_NAME \
       --tag $DOCKER_IMAGE \
       .
docker push $DOCKER_IMAGE
