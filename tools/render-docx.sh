#!/bin/bash
cd inst
for qmd in *.qmd
do
    sed -i 's/\.\.\/data-raw/https:\/\/raniere-phd\.gitlab\.io\/data-raw/' $qmd
done
quarto render index.qmd --execute --to docx
